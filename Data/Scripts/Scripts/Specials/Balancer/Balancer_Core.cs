﻿using Sandbox.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using Sandbox.ModAPI;
using Scripts.Shared;
using Scripts.Specials.Messaging;
using SharedLib;
using Slime;
using VRage.Game;
using VRage.Game.ModAPI;
using VRage.Utils;
using static Scripts.Specials.Balancer.Balancer_Commands;
using static Scripts.Specials.Balancer.RegDefinitions;
using BD = Sandbox.Definitions.MyCubeBlockDefinition;
using DATA = System.Collections.Generic.Dictionary<Sandbox.Definitions.MyCubeBlockDefinition, System.Collections.Generic.Dictionary<string, Scripts.Specials.Balancer.GetterSetter>>;
using SAVEDATA = System.Collections.Generic.Dictionary<VRage.ObjectBuilders.SerializableDefinitionId, System.Collections.Generic.Dictionary<string, double>>;
using VRage.Game.Components;

namespace Scripts.Specials.Balancer
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation | MyUpdateOrder.AfterSimulation)]
    class Balancer_Core : MySessionComponentBase {
        public static Connection<Command> connection;
        [ProtoContract]
        public class Command
        {
            [ProtoMember(1)] public Mode Mode;
            [ProtoMember(2)] public string Definitions;
            [ProtoMember(3)] public string Property;
            [ProtoMember(4)] public double Value;
        }
        public enum Mode { SET, SETMASS, MLT, CLR, POWTIER }
        public static readonly BlockIdMatcher idMatcher = new BlockIdMatcher();
        public static readonly DATA InitSettings = new DATA();
        private static SAVEDATA SavedSettings = new SAVEDATA();

        public override void Init(MyObjectBuilder_SessionComponent sessionComponent)
        {
            MyAPIGateway.Utilities.MessageEnteredSender += Utilities_MessageEnteredSender;
            connection = new Connection<Command>(38458, InitializeChanges);
            Common.SendChatMessage ("Balancer started");
            foreach (var t in MyDefinitionManager.Static.GetAllDefinitions().Select(Def => Def as BD)) { Register(t); }
            Common.SendChatMessage("Balancer finished");
            initCommands();
        }

        protected override void UnloadData()
        {
            MyAPIGateway.Utilities.MessageEnteredSender -= Utilities_MessageEnteredSender;
        }

        private static void InitializeChanges(Command data, ulong SteamUserId, bool isFromServer)
        {
            idMatcher.BlockIdMatchers(data.Definitions);
            
            foreach (var def in InitSettings.Keys.Where(idMatcher.Matches))
            {
                GetterSetter gs;
                if (!InitSettings[def].TryGetValue(data.Property, out gs))
                {
                    //Common.SendChatMessageToMe("Property not registered for Subtype: " + def.Id.SubtypeName, "Balancer");
                    continue;
                }
                
                var NewValue = ApplyChanges(data.Mode, gs, data.Value);
                EditSaveSettings(def.Id, data.Property, NewValue, data.Mode != Mode.CLR);
            }
            
            idMatcher.Clear();
            
            if (!isFromServer)connection.SendMessageToOthers(data);
        }
        private static void EditSaveSettings(MyDefinitionId id, string Property, double Value, bool isCLR)
        {
            if (SavedSettings.ContainsKey(id))
            { 
                if (SavedSettings[id].ContainsKey(Property))
                { 
                    if (isCLR) SavedSettings[id][Property] = Value;
                    else SavedSettings[id].Remove(Property);
                }
                else SavedSettings[id].Add(Property, Value);
            }
            else SavedSettings.Add(id, new Dictionary<string, double> {{Property, Value}});
        }
        private static void Utilities_MessageEnteredSender(ulong sender, string messageText, ref bool sendToOthers)
        {
            if(sender.PromoteLevel() < MyPromoteLevel.Admin || !messageText.StartsWith("!balancer") && !messageText.StartsWith("!bl")) return;
            sendToOthers = false;
            
            var data = messageText.Replace("!balancer ", "").Replace("!bl ", "").Split(' ');
            var cmd = data[0].ToLower();
            if (commands.ContainsKey(cmd))
            {
                try
                {
                    commands[cmd].Invoke(data);
                }
                catch (Exception e)
                {
                    Common.SendChatMessageToMe("Balancer::Utilities_MessageEntered: " + e, "Balancer");
                }
            }
            else
            {
                Common.SendChatMessageToMe("Command not found", "Balancer");
                //CommandHelp();
            }
        }
        private static double ApplyChanges(Mode mode, GetterSetter GS, double value = 1)
        {
            switch (mode)
            {
                case (Mode.SET): GS.Set(value); break;
                case (Mode.SETMASS): GS.Set(value * GS.GetMass()); break;
                case (Mode.MLT): GS.Mlt(value); break;
                case (Mode.CLR): GS.SetDefault(); break;
                case (Mode.POWTIER): GS.Mlt(Math.Pow(value, GS.GetTier()-1)); break;
            }
            return GS.Get();
        }
        public static void Load_Settings(string name)
        {
            try
            {
                if (MyAPIGateway.Utilities.FileExistsInWorldStorage(name+"_balance.xml", typeof(Balancer_Core)))
                {
                    using (var reader = MyAPIGateway.Utilities.ReadFileInWorldStorage(name + "_balance.xml", typeof(Balancer_Core)))
                    {
                        var b64 = MyAPIGateway.Utilities.SerializeFromXML<string>(reader.ReadToEnd());
                        var bytes = Convert.FromBase64String(b64);
                        SavedSettings = MyAPIGateway.Utilities.SerializeFromBinary<SAVEDATA>(bytes);
                        
                        MyLog.Default.WriteLine("Balancer successfully loaded saved settings");
                    }

                    foreach (var init in InitSettings)
                    {
                        foreach (var set in SavedSettings.Where(save => init.Key.Id == save.Key).SelectMany(save => save.Value))
                        {
                            init.Value[set.Key].Set(set.Value);
                        }
                    }
                }
                else Save_Settings(name);
            }
            catch (Exception e)
            {
                MyLog.Default.WriteLine("Balancer ERROR: " + e);
                Common.SendChatMessageToMe("Balancer ERROR: " + e);
            }
        }
        public static void Save_Settings(string name)
        {
            try
            {
                var bytes = MyAPIGateway.Utilities.SerializeToBinary(SavedSettings);
                var b64 = Convert.ToBase64String(bytes);
                using (var writer = MyAPIGateway.Utilities.WriteFileInWorldStorage(name + "_balance.xml", typeof(Balancer_Core)))
                {
                    writer.Write(MyAPIGateway.Utilities.SerializeToXML(b64));
                }
            }
            catch (Exception e)
            {
                MyLog.Default.WriteLine("Balancer ERROR: " + e);
                Common.SendChatMessageToMe("Balancer ERROR: " + e);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Sandbox.Definitions;
using Scripts.Specials.Messaging;
using VRage;
using VRage.Game;
using static Scripts.Specials.Balancer.Balancer_Core;

namespace Scripts.Specials.Balancer
{
    public class RegDefinitions
    {
        public static void Register(MyCubeBlockDefinition bd)
        {
            try
            {
                Register2(bd);
            } 
            catch (Exception e)
            {
                Common.SendChatMessage ("Error at:" + bd.Id);
            }
        }

        private static void Register2 (MyCubeBlockDefinition bd)
        {
            var th = bd as MyThrustDefinition;
            if (th != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"ForceMagnitude", new GetterSetter(bd,() => th.ForceMagnitude, (y) => th.ForceMagnitude = (float) y)},
                    {"MaxPowerConsumption", new GetterSetter(bd,() => th.MaxPowerConsumption, (y) => th.MaxPowerConsumption = (float) y)},
                    {"MinPowerConsumption", new GetterSetter(bd,() => th.MinPowerConsumption, (y) => th.MinPowerConsumption = (float) y)},
                    {"EffectivenessAtMaxInfluence", new GetterSetter(bd,() => th.EffectivenessAtMaxInfluence, (y) => th.EffectivenessAtMaxInfluence = (float) y)},
                    {"EffectivenessAtMinInfluence", new GetterSetter(bd,() => th.EffectivenessAtMinInfluence, (y) => th.EffectivenessAtMinInfluence = (float) y)},
                    {"MaxPlanetaryInfluence", new GetterSetter(bd,() => th.MaxPlanetaryInfluence, (y) => th.MaxPlanetaryInfluence = (float) y)},
                    {"MinPlanetaryInfluence", new GetterSetter(bd,() => th.MinPlanetaryInfluence, (y) => th.MinPlanetaryInfluence = (float) y)},
                    {"FlameDamage", new GetterSetter(bd,() => th.FlameDamage, (y) => th.FlameDamage = (float) y)},
                    {"SlowdownFactor", new GetterSetter(bd,() => th.SlowdownFactor, (y) => th.SlowdownFactor = (float) y)},
                    {"ConsumptionFactorPerG", new GetterSetter(bd,() => th.ConsumptionFactorPerG, (y) => th.ConsumptionFactorPerG = (float) y)},
                    {"FlameLengthScale", new GetterSetter(bd,() => th.FlameLengthScale, (y) => th.FlameLengthScale = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var engine = bd as MyHydrogenEngineDefinition;
            if (engine != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxPowerOutput", new GetterSetter(bd,() => engine.MaxPowerOutput, (y) => engine.MaxPowerOutput = (float) y)},
                    {"FuelCapacity", new GetterSetter(bd,() => engine.FuelCapacity, (y) => engine.FuelCapacity = (float) y)},
                    {"FuelProductionToCapacityMultiplier", new GetterSetter(bd,() => engine.FuelProductionToCapacityMultiplier, (y) => engine.FuelProductionToCapacityMultiplier = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var oxygen = bd as MyOxygenGeneratorDefinition;
            if (oxygen != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"InventoryMaxVolume", new GetterSetter(bd,() => oxygen.InventoryMaxVolume, (y) => oxygen.InventoryMaxVolume = (float) y)},
                    {"StandbyPowerConsumption", new GetterSetter(bd,() => oxygen.StandbyPowerConsumption, (y) => oxygen.StandbyPowerConsumption = (float) y)},
                    {"OperationalPowerConsumption", new GetterSetter(bd,() => oxygen.OperationalPowerConsumption, (y) => oxygen.OperationalPowerConsumption = (float) y)},
                    {"IceConsumptionPerSecond", new GetterSetter(bd,() => oxygen.IceConsumptionPerSecond, (y) => oxygen.IceConsumptionPerSecond = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var uraniumgen = bd as MyReactorDefinition;
            if (uraniumgen != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxPowerOutput", new GetterSetter(bd,() => uraniumgen.MaxPowerOutput, (y) => uraniumgen.MaxPowerOutput = (float) y)},
                    {"FuelProductionToCapacityMultiplier", new GetterSetter(bd,() => uraniumgen.FuelProductionToCapacityMultiplier, (y) => uraniumgen.FuelProductionToCapacityMultiplier = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var wind = bd as MyWindTurbineDefinition;
            if (wind != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxPowerOutput", new GetterSetter(bd,() => wind.MaxPowerOutput, (y) => wind.MaxPowerOutput = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var solar = bd as MySolarPanelDefinition;
            if (solar != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxPowerOutput", new GetterSetter(bd,() => solar.MaxPowerOutput, (y) => solar.MaxPowerOutput = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }


            var oxyfarm = bd as MyOxygenFarmDefinition;
            if (oxyfarm != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"OperationalPowerConsumption", new GetterSetter(bd,() => oxyfarm.OperationalPowerConsumption, (y) => oxyfarm.OperationalPowerConsumption = (float) y)},
                    {"MaxGasOutput", new GetterSetter(bd,() => oxyfarm.MaxGasOutput, (y) => oxyfarm.MaxGasOutput = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var tank = bd as MyGasTankDefinition;
            if (tank != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"Capacity", new GetterSetter(bd,() => tank.Capacity, (y) => tank.Capacity = (float) y)},
                    {"OperationalPowerConsumption", new GetterSetter(bd,() => tank.OperationalPowerConsumption, (y) => tank.OperationalPowerConsumption = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }


            var bat = bd as MyBatteryBlockDefinition;
            if (bat != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxPowerOutput", new GetterSetter(bd,() => bat.MaxPowerOutput, (y) => bat.MaxPowerOutput = (float) y)},
                    {"RequiredPowerInput", new GetterSetter(bd,() => bat.RequiredPowerInput, (y) => bat.RequiredPowerInput = (float) y)},
                    {"MaxStoredPower", new GetterSetter(bd,() => bat.MaxStoredPower, (y) => bat.MaxStoredPower = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var gyro = bd as MyGyroDefinition;
            if (gyro != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"ForceMagnitude", new GetterSetter(bd,() => gyro.ForceMagnitude, (y) => gyro.ForceMagnitude = (float) y)},
                    {"RequiredPowerInput", new GetterSetter(bd,() => gyro.ForceMagnitude, (y) => gyro.ForceMagnitude = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var refine = bd as MyRefineryDefinition;
            if (refine != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"StandbyPowerConsumption", new GetterSetter(bd,() => refine.StandbyPowerConsumption, (y) => refine.StandbyPowerConsumption = (float) y)},
                    {"OperationalPowerConsumption", new GetterSetter(bd,() => refine.OperationalPowerConsumption, (y) => refine.OperationalPowerConsumption = (float) y)},
                    {"RefineSpeed", new GetterSetter(bd,() => refine.RefineSpeed, (y) => refine.RefineSpeed = (float) y)},
                    {"OreAmountPerPullRequest", new GetterSetter(bd,() => (float)refine.OreAmountPerPullRequest, (y) => refine.OreAmountPerPullRequest = (MyFixedPoint) y)},
                    {"InventoryMaxVolume", new GetterSetter(bd,() => refine.InventoryMaxVolume, (y) => refine.InventoryMaxVolume = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var susp = bd as MyMotorSuspensionDefinition;
            if (susp != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"MaxHeight", new GetterSetter(bd,() => susp.MaxHeight, (y) => susp.MaxHeight = (float) y)},
                    {"MinHeight", new GetterSetter(bd,() => susp.MinHeight, (y) => susp.MinHeight = (float) y)},
                    {"MaxForceMagnitude", new GetterSetter(bd,() => susp.MaxForceMagnitude, (y) => susp.MaxForceMagnitude = (float) y)},
                    {"PropulsionForce", new GetterSetter(bd,() => susp.PropulsionForce, (y) => susp.PropulsionForce = (float) y)},
                    {"RequiredPowerInput", new GetterSetter(bd,() => susp.RequiredPowerInput, (y) => susp.RequiredPowerInput = (float) y)},
                    {"AxleFriction", new GetterSetter(bd,() => susp.AxleFriction, (y) => susp.AxleFriction = (float) y)},
                    {"AirShockMinSpeed", new GetterSetter(bd,() => susp.AirShockMinSpeed, (y) => susp.AirShockMinSpeed = (float) y)},
                    {"AirShockMaxSpeed", new GetterSetter(bd,() => susp.AirShockMaxSpeed, (y) => susp.AirShockMaxSpeed = (float) y)},
                    {"MaxSteer", new GetterSetter(bd,() => susp.MaxSteer, (y) => susp.MaxSteer = (float) y)},
                    {"SteeringSpeed", new GetterSetter(bd,() => susp.SteeringSpeed, (y) => susp.SteeringSpeed = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var jd = bd as MyJumpDriveDefinition;
            if (jd != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    {"RequiredPowerInput", new GetterSetter(bd,() => jd.RequiredPowerInput, (y) => jd.RequiredPowerInput = (float) y)},
                    {"PowerNeededForJump", new GetterSetter(bd,() => jd.PowerNeededForJump, (y) => jd.PowerNeededForJump = (float) y)},
                    {"MaxJumpMass", new GetterSetter(bd,() => jd.MaxJumpMass, (y) => jd.MaxJumpMass = (float) y)},
                };
                InitSettings.Add(bd, dict);
                return;
            }

            var ass = bd as MyAssemblerDefinition;
            if (ass != null)
            {
                var dict = new Dictionary<string, GetterSetter>
                {
                    //{"AssemblySpeed", new GetterSetter(bd,() => ass.AssemblySpeed, (y) => ass.AssemblySpeed = (float) y)},
                    {"StandbyPowerConsumption", new GetterSetter(bd,() => ass.StandbyPowerConsumption, (y) => ass.StandbyPowerConsumption = (float) y)},
                    {"OperationalPowerConsumption", new GetterSetter(bd,() => ass.OperationalPowerConsumption, (y) => ass.OperationalPowerConsumption = (float) y)},
                    {"InventoryMaxVolume", new GetterSetter(bd,() => ass.InventoryMaxVolume, (y) => ass.InventoryMaxVolume = (float) y)}
                };
                InitSettings.Add(bd, dict);
                return;
            }
        }

    }
}
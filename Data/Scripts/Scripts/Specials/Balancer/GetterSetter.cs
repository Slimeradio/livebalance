﻿using Sandbox.Definitions;
using Slime;
using System;

namespace Scripts.Specials.Balancer
{
    public class GetterSetter
    {
        private readonly double DefaultValue;
        private readonly double Mass;
        private readonly Func<double> Getter;
        private readonly Action<double> Setter;
        private readonly MyCubeBlockDefinition def;

        public GetterSetter(MyCubeBlockDefinition def, Func<double> Getter, Action<double> Setter)
        {
            this.Getter = Getter;
            this.Setter = Setter;
            DefaultValue = Get();
            Mass = def.GetMass();
        }



        public double Get()
        {
            return Getter.Invoke();
        }

        public bool IsModified()
        {
            return (Math.Abs(DefaultValue - Get()) > float.Epsilon);
        }

        public void Set(double value)
        {
            Setter.Invoke(value);
        }

        public void SetDefault()
        {
            Set(DefaultValue);
        }

        public void Mlt(double value)
        {
            Set(Get() * value);
        }

        public double GetTier()
        {
            if (def.Id.SubtypeName.Contains("T02")) return 2d;
            if (def.Id.SubtypeName.Contains("T03")) return 3d;
            if (def.Id.SubtypeName.Contains("T04")) return 4d;
            if (def.Id.SubtypeName.Contains("T05")) return 5d;
            if (def.Id.SubtypeName.Contains("T06")) return 6d;
            if (def.Id.SubtypeName.Contains("T07")) return 7d;
            if (def.Id.SubtypeName.Contains("T08")) return 8d;
            if (def.Id.SubtypeName.Contains("T09")) return 9d;
            if (def.Id.SubtypeName.Contains("T10")) return 10d;
            if (def.Id.SubtypeName.Contains("T11")) return 11d;
            if (def.Id.SubtypeName.Contains("T12")) return 12d;

            return 1d;
        }

        public double GetMass()
        {
            return Mass;
        }
    }
}
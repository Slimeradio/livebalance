﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Specials.Messaging;
using static Scripts.Specials.Balancer.Balancer_Core;

namespace Scripts.Specials.Balancer
{
    public class Balancer_Commands
    {
        public static readonly Dictionary<string, Action<string[]>> commands = new Dictionary<string, Action<string[]>>();
        public static void initCommands()
        {
            commands.Add("list", DefinitionList);
            commands.Add("help", CommandHelp);
            commands.Add("set", CommandCheck);
            commands.Add("setmass", CommandCheck);
            commands.Add("mlt", CommandCheck);
            commands.Add("powtier", CommandCheck);
            commands.Add("clr", CommandCheck);
            commands.Add("changes", CommandChanges);
            commands.Add("save", CommandSave);
            commands.Add("load", CommandLoad);
        }
        private static void DefinitionList(string[] data)
        {
            Common.SendChatMessageToMe("Registered def. count: " + InitSettings.Count, "Balancer");
            foreach (var Key in InitSettings.Keys)
            { 
                Common.SendChatMessageToMe(Key.ToString(), "Balancer");
            }
        }
        private static void CommandCheck(string[] data)
        {
            Mode mode;
            if (!Enum.TryParse(data[0].ToUpper(), out mode))
            {
                Common.SendChatMessageToMe("Incorrect action", "Balancer");
                return;
            }
            
            if (data.Length < 2)
            {
                Common.SendChatMessageToMe("Property not set", "Balancer");
                return;
            }
            
            double value = 1;
            var _x = 3;
            
            if (mode != Mode.CLR) double.TryParse(data[2], out value);
            else _x = 2;

            var definitions = ""; 
            for (var x = _x; x < data.Length; x++)
            {
                definitions += data[x] + " ";
            }
            
            idMatcher.BlockIdMatchers(definitions);

            if (!InitSettings.Keys.Where(idMatcher.Matches).Any())
            {
                Common.SendChatMessageToMe("definition not found", "Balancer");
                return;
            }
            idMatcher.Clear();

            connection.SendMessageToServer(new Command {Mode = mode, Definitions = definitions, Property = data[1], Value = value});
        }
        public static void CommandHelp(string[] data = null)
        {
            Common.SendChatMessageToMe("!bl help", "Balancer");
            Common.SendChatMessageToMe("!bl list - list of all definitions", "Balancer");
            Common.SendChatMessageToMe("!bl clr ForceMagnitude Thruster/*  - return all values to default", "Balancer");
            Common.SendChatMessageToMe("!bl mlt ForceMagnitude 2 Thruster/*  - multiplies setting <ForceMagnitude> by <Value>", "Balancer");
            Common.SendChatMessageToMe("!bl set ForceMagnitude 2.5 Thruster/*  - will set <ForceMagnitude> exact <Value>", "Balancer");
            Common.SendChatMessageToMe("!bl setmass ForceMagnitude 2.5 Thruster/*  - will set <ForceMagnitude> exact <Value * Mass of block>", "Balancer");
            Common.SendChatMessageToMe("!bl powtier ForceMagnitude 2.5 Thruster/*  - multiplies setting <ForceMagnitude> by <Value^tier of block>", "Balancer");
            Common.SendChatMessageToMe("!bl changes - Show all changes done", "Balancer");
            Common.SendChatMessageToMe("!bl save <Name> - to save settings", "Balancer");
            Common.SendChatMessageToMe("!bl load <Name> - to load settings", "Balancer");
        }
        private static void CommandSave(string[] data = null) { Save_Settings(data[0]); }
        private static void CommandLoad(string[] data = null) { Load_Settings(data[0]); }
    }
}